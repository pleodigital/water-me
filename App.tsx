import React from 'react';
import Provider from './src/redux/ReduxWrapper'
import {NavigationContainer} from "@react-navigation/native";
import LeftPanel from "./src/components/navigation/LeftPanel";
import TabNavigation from "./src/components/navigation/TabNavigation";
import StackNavigator from "./src/components/navigation/StackNavigation";
import {createDrawerNavigator} from "@react-navigation/drawer";
import { useFonts } from 'expo-font';
import {ThemeProvider} from "styled-components";
import {theme} from "./src/colors";
import LoginScreen from "./src/pages/LoginScreen";

const DrawerNav = createDrawerNavigator();

export default function App() {

    const [loaded] = useFonts({
        Dosis: require('./assets/fonts/Dosis-Medium.ttf'),
    });
    if (!loaded) {
        return null;
    }

    return (
        <ThemeProvider theme={theme}>
        <Provider>
            <NavigationContainer>
                <DrawerNav.Navigator drawerContent={props => (<LeftPanel {...props} />)}>
                    <DrawerNav.Screen name='MainPage' component={TabNavigation} />
                    <DrawerNav.Screen name='StackNavigator'>
                        {props => <StackNavigator {...props} />}
                    </DrawerNav.Screen>
                    <DrawerNav.Screen name='LoginScreen' component={LoginScreen}
                      options={{
                          title: 'Zaloguj się :D'
                      }}
                    />
                    {/*<DrawerNav.Screen name='FavouritePage' component={FavouritePage} />*/}
                    {/*<DrawerNav.Screen name='SettingsPage' component={SettingsPage} />*/}
                </DrawerNav.Navigator>
            </NavigationContainer>
        </Provider>
        </ThemeProvider>
    );
};
