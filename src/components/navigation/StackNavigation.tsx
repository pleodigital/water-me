import {View, Text} from "react-native";
import React from "react";

// @ts-ignore
const StackNavigator = ({route}) => {

    const {recipe} = route.params;

    return (
        <View>
            <Text>
                {recipe?.name}
            </Text>
            <Text>
                {recipe?.description}
            </Text>
        </View>
    )
}
export default StackNavigator
