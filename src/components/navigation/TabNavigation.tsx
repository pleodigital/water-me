import MainScreen from "../../pages/MainScreen";
import {Image, View} from "react-native";
import SettingsScreen from "../../pages/SettingsScreen";
import React from "react";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import styled from "styled-components";
import homeIcon from '../../Images/navigation/home.png';
import settingsIcon from '../../Images/navigation/settings.png';

const TabNavigation = () => {
    const Tab = createMaterialTopTabNavigator();
    return (
            <Tab.Navigator
                tabBarPosition="bottom"
                tabBarOptions={{
                    showIcon: true,
                    showLabel: false,
                    labelStyle: {fontSize: 12},
                    tabStyle: {zIndex: 9999,},
                    activeTintColor: 'blue',
                    inactiveTintColor: 'grey',
                    iconStyle: {height: 25, width: 25,},
                    style: {backgroundColor: '#696969'},
                }}
            >
                <Tab.Screen name="MainPage" component={MainScreen}
                    options={{
                        tabBarIcon: ({focused}) => (
                            <View>
                                <StyledNavIcon
                                    source={homeIcon}
                                    style={{
                                        tintColor: focused ? '#DCDCDC' : 'black',
                                    }}
                                />
                            </View>
                        )
                    }}
                />
                <Tab.Screen name="SettingsPage" component={SettingsScreen}
                    options={{
                        tabBarIcon: ({focused}) => (
                            <View>
                                <StyledNavIcon
                                    source={settingsIcon}
                                    style={{
                                        tintColor: focused ? '#DCDCDC' : 'black',
                                    }}
                                />
                            </View>
                        )
                    }}
                />
            </Tab.Navigator>
    )
}

export default TabNavigation

const StyledNavIcon  = styled(Image)`
  width: 25px;
  height: 25px;
`;
