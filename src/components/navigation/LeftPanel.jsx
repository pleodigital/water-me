import {View} from "react-native";
import React from "react";
import styled from "styled-components";
import CustomButton from "../common/custom-button/CustomButton";
import {useNavigation} from "@react-navigation/core";


const LeftPanel = () => {

    const navigation = useNavigation();


    return (
        <LeftPanelWrapper>
            <ButtonsWrapper>
                <CustomButton margin="0px 0px 20px 0px" text='zaloguj' onPress={()=>navigation.navigate('LoginScreen')} />
                <CustomButton  text='zarejestruj'  />
            </ButtonsWrapper>
        </LeftPanelWrapper>
    )
}
export default LeftPanel

const LeftPanelWrapper = styled(View)`
  padding: 30px 10px;
`;
const ButtonsWrapper = styled(View)`
  margin-top: 20px;
`;
