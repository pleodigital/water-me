export default interface ListItemInterface {
    name: string;
    description: string;
    item: object;
}
