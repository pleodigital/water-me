import React, {FunctionComponent} from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
// @ts-ignore
import styled from "styled-components";
// @ts-ignore
import TileImage from '../../Images/default-image.png'
import ListItemInterface from "./ListItem.interface";
import {useNavigation} from "@react-navigation/core";

const ListItem:FunctionComponent<ListItemInterface> = ({ item ,name, description}) => {

    const navigation = useNavigation();

    return (
        <ListItemWrapper onPress={()=>navigation.navigate('StackNavigator', {recipe: item})}
            style={{
                elevation: 8,
                shadowRadius: 60,
                shadowColor: '#000',
                shadowOffset: {
                    width: 8,
                    height: 8
                }
            }}
        >
            <ImageWrapper>
                <StyledImage source={TileImage} />
            </ImageWrapper>
            <ContentWrapper>
                <Text>{name}</Text>
                <Text>{description}</Text>
            </ContentWrapper>
        </ListItemWrapper>
    )
}
export default ListItem


const ListItemWrapper = styled(TouchableOpacity)`
  display: flex;
  margin: 0 auto 20px;
  flex-direction: row;
  width: 90%;
  z-index: 99999;
  background-color: #FFF;
  border-radius: 12px;
  padding: 10px 0;
`;
const ImageWrapper = styled(View)`
  width: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const StyledImage = styled(Image)`
  height: 150px;
  width: 150px;
`;

const ContentWrapper = styled(View)``;

