export default interface CustomButtonInterface {
    text: string;
    onPress: ()=> void;
    margin: string;
}
