// @ts-ignore
import styled from "styled-components";
import {Image, Text, TouchableOpacity, View} from "react-native";
import React, {FunctionComponent} from "react";
// @ts-ignore
import ButtonImage from '../../../Images/leaf.png'
import CustomButtonInterface from "./CustomButton.interface";

const CustomButton:FunctionComponent<CustomButtonInterface> = ({text, onPress, margin }) => {
    return (
        <CustomButtonWrapper onPress={onPress} margin={margin}>
            <CustomButtonContent>
                <CustomButtonText>
                    {text}
                </CustomButtonText>
                <CustomButtonImage source={ButtonImage} />
            </CustomButtonContent>
        </CustomButtonWrapper>
    )
}
export default CustomButton


const CustomButtonWrapper = styled(TouchableOpacity)`
  border-radius: 10px;
  overflow: hidden;
  border: 2px solid ${({theme}) => theme.colors.lightGreen};
  margin: ${props => props.margin || '0px'};
`;

const CustomButtonContent = styled(View)`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  padding: 20px;
`;

const CustomButtonImage = styled(Image)`
  width: 30px;
  height: 30px;
  margin-right: 20px;
  overflow: hidden;
`;

const CustomButtonText = styled(Text)`
  margin-right: 20px;
  letter-spacing: 2px;
  font-family: Dosis;
`;
