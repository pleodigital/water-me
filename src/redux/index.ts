import { combineReducers } from 'redux';
import plants from '../redux/plantReducer'
import user from '../redux/AuthorizeReducer'

export default combineReducers({
    plants,
    user,
});
