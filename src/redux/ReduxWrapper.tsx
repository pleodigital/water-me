import React from 'react';
import { applyMiddleware, createStore as reduxCreateStore } from 'redux';
import { Provider } from 'react-redux';
import promise from 'redux-promise-middleware';
import rootReducer from '.';

const middlewares = [promise];

const createStore = () => reduxCreateStore(rootReducer, applyMiddleware(...middlewares));

// @ts-ignore
export default ({ children }) => <Provider store={createStore()}>{children}</Provider>;
