import {submitUser} from "../../service";


const initialState = {
    error: null,
    isLoggedIn: null,
    login: null,
    password: null,
};
const SUBMIT_UER = 'SUBMIT_UER';


export const loadUser = (login: string, password: string) => {
    const test = {
        type: SUBMIT_UER,
        payload: submitUser(login, password),
    }
    test.payload.then((res) => console.log(res));
    return test
};


// @ts-ignore
export default (state = initialState, action) => {
    switch (action.type) {
        case `${SUBMIT_UER}_PENDING`:
            return {
                ...state,
                error: false
            };
        case `${SUBMIT_UER}_REJECTED`:
            return {
                ...state,
                error: true
            };
        case `${SUBMIT_UER}_FULFILLED`:
            return {
                ...state,
                isLoggedIn: action?.payload?.data,
                login: action?.login,
                password: action?.password,
                error: false
            };
        default:
            return {...state};
    }
};
