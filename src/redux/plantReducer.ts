import {getPlants} from "../../service";


const initialState = {
    error: null,
    plants: []
};
const GET_PLANTS = 'GET_PLANTS';


export const loadPlants = () => ({
    type: GET_PLANTS,
    payload: getPlants(),
});


// @ts-ignore
export default (state = initialState, action) => {
    switch (action.type) {
        case `${GET_PLANTS}_PENDING`:
            return {
                ...state,
                error: false
            };
        case `${GET_PLANTS}_REJECTED`:
            return {
                ...state,
                error: true
            };
        case `${GET_PLANTS}_FULFILLED`:
            return {
                ...state,
                plants: action.payload.data
            };
        default:
            return { ...state };
    }
};
