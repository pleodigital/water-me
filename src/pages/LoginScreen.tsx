import {Image, Text, TextInput, View} from "react-native";
import React, {useState} from "react";
import styled from "styled-components";
import LeafImage from '../Images/leaf.png'
import CustomButton from "../components/common/custom-button/CustomButton";
import { useDispatch, useSelector } from "react-redux";
import { loadUser } from "../redux/AuthorizeReducer";

const LoginScreen = () => {

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    const isUserLogged = useSelector(state => state?.isLoggedIn?.isLoggedIn)

    const loginUser = () => {
        console.log(login, password)
        dispatch(loadUser(login, password))
    }



    return (
        <PageContentWrapper>
            {
                isUserLogged && (<Text>Jest kurde zalogowany :DDDDD</Text>)
            }
            <FormWrapper>
                <FormLabelWrapper>
                    <FormImage source={LeafImage} />
                    <FormLabel>login</FormLabel>
                </FormLabelWrapper>
                <LoginField
                    placeholder='wpisuj coś'
                    onChangeText={(val)=> setLogin(val)}
                />
                <FormLabelWrapper>
                    <FormImage source={LeafImage} />
                    <FormLabel>hasło</FormLabel>
                </FormLabelWrapper>
                <LoginField
                    placeholder='wpisuj coś'
                    onChangeText={(val) => setPassword(val)}
                />
                <CustomButton margin="80px 0px 0px 0px" text='zaloguj się' onPress={()=>loginUser()} />
            </FormWrapper>


        </PageContentWrapper>
    )
}
export default LoginScreen


const PageContentWrapper = styled(View)`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;
  background-color: #FFF;
  height: 100%;
`;
const FormWrapper = styled(View)`
  padding: 0 20px;
`;


const FormLabelWrapper = styled(View)`
  display: flex;
  flex-direction: row;
  margin-bottom: 10px;
`;
const FormLabel = styled(Text)`
  margin-left: 10px;
`;

const FormImage = styled(Image)`
  width: 20px;
  height: 20px;
`;

const LoginField = styled(TextInput)`
  border-radius: 12px;
  overflow: hidden;
  margin-bottom: 30px;
  padding: 15px 25px;
  border: 1px solid ${({theme}) => theme.colors.lightGreen};
`;
