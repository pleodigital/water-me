import {FlatList, View} from "react-native";
import React, {useEffect} from "react";
import {loadPlants} from "../redux/plantReducer";
import {useDispatch, useSelector} from "react-redux";
import ListItem from "../components/list-item/ListItem";
import styled from "styled-components";

const MainScreen = () => {

    const dispatch = useDispatch();
    const plantsData = useSelector(state => state.plants.plants)


    useEffect(() => {
       dispatch(loadPlants())
    })

    return (
        <>
            <PageContentWrapper>
            <StyledFlatList
                data={plantsData}
                showsVerticalScrollIndicator={false}
                renderItem={ ({item}) => ( <ListItem name={item.name} description={item.description} item={item} /> )}
                keyExtractor={(item) => item.id}
            />
            </PageContentWrapper>
        </>
    )
}
export default MainScreen

const PageContentWrapper = styled(View)`
  width: 100%;
  background-color: #FFF;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const StyledFlatList = styled(FlatList)`
`;
