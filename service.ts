import axios from 'axios';

const URL = 'https://612f24d45fc50700175f143e.mockapi.io'

export const getPlants = () => {
    return axios.get(`${URL}/plants1`)
}

// export const submitUser = (login: string, password: string) => {
//     return axios.post(`${URL}/users`, {login, password});
// }
export const submitUser = (login: string, password: string) => axios.post(`${URL}/authorize`,  {login, password});


